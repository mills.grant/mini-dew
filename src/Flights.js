import {useEffect, useState} from "react";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";

function FlightRow({flight, user, onFlightAdd}) {
    async function onDelete(id) {
        await axios.delete(`http://localhost:8480/flights/${user}/${id}`);
        //After typescript is implemented use data type as argument to get call
        let getResponse = await axios.get(`http://localhost:8480/flights/${user}`);
        onFlightAdd(getResponse.data)
    }

    async function onEdit(flight) {
        //Prepopulate values from flight
        const distanceEl = document.getElementById("distance_edit");
        const segmentsEl = document.getElementById("segments_edit");
        const flightDateEl = document.getElementById("flightDate_edit");
        const idEl = document.getElementById("flightId");
        distanceEl.setAttribute("value", flight.amount);
        segmentsEl.setAttribute("value", flight.flightSegments);
        flightDateEl.setAttribute("value", flight.date)
        idEl.setAttribute("value", flight.id);
        document.getElementById("my_edit_modal").showModal()
    }

    return (
        <tr>
            <td className="p-5 border border-b-slate-700">{flight.amount}</td>
            <td className="p-5 border border-b-slate-700">{flight.flightSegments}</td>
            <td className="p-5 border border-b-slate-700">{flight.date}</td>
            <td className="p-5 border border-b-slate-700">
                <label htmlFor="my_modal_6" className="btn" onClick={() => {onEdit(flight)}}>Edit</label>
                <input type="checkbox" id="my_modal_6" className="modal-toggle"/>
            </td>
            <td className="p-5 border border-b-slate-700"><button className="btn" onClick={() => {onDelete(flight.id)}}>Delete</button></td>
        </tr>
    );
}

function FlightsTable({flights, user, onFlightAdd}) {
    const rows = [];

    flights.forEach((flight) => {
        rows.push(
            <FlightRow
                flight={flight}
                user={user}
                onFlightAdd={onFlightAdd}
                key={flight.id}/>
        );
    });

    async function onEditSubmit() {
        // Gather data about flight from inputs
        // TODO: Wrap inputs in a form, use onSubmit, pass it a function, parse function data based on name
        const distance = document.getElementById("distance_edit").value;
        const segments = document.getElementById("segments_edit").value;
        const flightDate = document.getElementById("flightDate_edit").value;
        // Create a unique id
        const id = document.getElementById("flightId").value;
        // Post to /flight
        await axios.put(`http://localhost:8480/flights/${user}/${id}`, {
            "amount": distance,
            "flightSegments": segments,
            "date": flightDate,
            "userId": user
        })
        let getResponse = await axios.get(`http://localhost:8480/flights/${user}`);
        onFlightAdd(getResponse.data);
    }

    return (
        <div className="p-9 space-y-5">
            <table className="table-auto border-collapse">
                <thead>
                <tr>
                    <th className="border border-slate-700 p-5">Miles</th>
                    <th className="border border-slate-700 p-5">Flight Segments</th>
                    <th className="border border-slate-700 p-5">Date</th>
                    <th className="border border-slate-700 p-5">Edit</th>
                    <th className="border border-slate-700 p-5">Delete</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
            <dialog id="my_edit_modal" className="modal">
                <div className="modal-box">
                    <div>
                        <label className="label">Total Distance*</label>
                        <input className="input input-bordered input-accent" type="text" id="distance_edit" placeholder="Total Distance*"/>
                    </div>
                    <div>
                        <label className="label">Flight Segments*</label>
                        <input className="input input-bordered input-accent" type="text" id="segments_edit" placeholder="Total Segments*"/>
                    </div>
                    <div>
                        <label className="label">Flight Date</label>
                        <input className="input input-bordered input-accent" type="date" id="flightDate_edit" placeholder="Flight Date"/>
                    </div>
                    <input className="input" hidden={true} id="flightId" />
                    <div className="modal-action">
                        <form method="dialog">
                            <button className="btn bg-green-700 hover:bg-green-600" onClick={onEditSubmit}>Submit</button>
                        </form>
                    </div>
                </div>
            </dialog>
        </div>
    );
}

function AddFlightSection({onFlightAdd, user}) {
    function onClick() {
        // Gather data about flight from inputs
        // TODO: Wrap inputs in a form, use onSubmit, pass it a function, parse function data based on name
        const distance = document.getElementById("distance").value;
        const segments = document.getElementById("segments").value;
        const flightDate = document.getElementById("flightDate").value;
        // Create a unique id
        const id = uuidv4();
        // Post to /flight
        console.log(distance);
        axios.post("http://localhost:8480/flights", {
            "amount": distance,
            "flightSegments": segments,
            "date": flightDate,
            "id": id,
            "userId": user
        }).then(function (response) {
            console.log(response);
            axios.get(`http://localhost:8480/flights/${user}`).then(function(response){
                console.log(response);
                onFlightAdd(response.data);
            })
        });
    }

    return (
        <div className="space-y-5 max-w-sm p-9">
            <div>
                <label className="label">Total Distance*</label>
                <input className="input input-bordered input-accent" type="text" id="distance" placeholder="Total Distance*"/>
            </div>
            <div>
                <label className="label">Flight Segments*</label>
                <input className="input input-bordered input-accent" type="text" id="segments" placeholder="Total Segments*"/>
            </div>
            <div>
                <label className="label">Flight Date</label>
                <input className="input input-bordered input-accent" type="date" id="flightDate" placeholder="Flight Date"/>
            </div>
            <button className="btn bg-green-700 hover:bg-green-600" onClick={onClick}>Submit</button>
        </div>
    );
}

function FlightPage({flights, onFlightAdd, user}) {

    return (
        <div className="flex flex-wrap">
            <AddFlightSection flights={flights} user={user} onFlightAdd={onFlightAdd} />
            <FlightsTable
            flights={flights} user={user} onFlightAdd={onFlightAdd}
            />
        </div>
    );
}

export default function Flights() {
    const [user, setUser] = useState("1");
    const [flights, setFlights] = useState([]);
    //Leave here or else memory leak?
    useEffect(() => {
        axios.get(`http://localhost:8480/flights/${user}`).then(function (response) {
            setFlights(response.data);
        });
    }, [setFlights]);

    return <FlightPage flights={flights} onFlightAdd={setFlights} user={user} />;
}
