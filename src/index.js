import React, {StrictMode} from "react";
import {
    createBrowserRouter,
    RouterProvider,
    Outlet, Link
} from "react-router-dom"
import "./styles.css";
import "./main.css";

import Flights from "./Flights";
import Usages from "./Usages"
import {createRoot} from "react-dom/client";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChartLine, faDroplet, faPlane, faPlug, faUser} from "@fortawesome/free-solid-svg-icons";

function Layout() {
    return (
        <>
            <header className="navbar bg-base-100 bg-green-700">
                <div className="navbar-start">
                    <button className="btn btn-ghost btn-square">
                        <Link to="/">
                            <FontAwesomeIcon icon={faDroplet} /> DEW
                        </Link>
                    </button>
                </div>
                <div className="navbar-end">
                    <div className="dropdown dropdown-end">
                        <label tabIndex={0} className="btn btn-ghost btn-square">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d="M4 6h16M4 12h16M4 18h7"
                                />
                            </svg>
                        </label>
                        <ul
                            tabIndex={0}
                            className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-300 rounded-box w-52"
                        >
                            <li>
                                <Link to="/">
                                    <div>
                                        <FontAwesomeIcon icon={faChartLine} /> Dashboard
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/usages">
                                    <div>
                                        <FontAwesomeIcon icon={faPlug} /> My Resource Usages
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/flights">
                                    <div>
                                        <FontAwesomeIcon icon={faPlane} /> My Flights
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/profile">
                                    <div>
                                        <FontAwesomeIcon icon={faUser} /> My Profile
                                    </div>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <main>
                <Outlet />
            </main>
        </>
    )
}

function App() {
    // initialize a browser router
    const router = createBrowserRouter([{
        element: <Layout />,
        children: [
            {
                path: "/flights",
                element: <Flights />,
            },
            // other pages....
            {
                path: "/usages",
                element: <Usages />,
            }
        ]
    }])

    return (
        <RouterProvider router={router} />
    )
}

export default App

const root = createRoot(document.getElementById("root"));
root.render(
    <StrictMode>
        <App />
    </StrictMode>
);

