import {useEffect, useState} from "react";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import {
    createColumnHelper,
    getCoreRowModel,
    getFilteredRowModel, getSortedRowModel,
    useReactTable
} from '@tanstack/react-table';

function UsageRow({user, rowData, setData}) {
    function onDelete(id) {
        axios.delete(`http://localhost:8480/resource-usage/${user}/${id}`)
            .then(function () {
                axios.get(`http://localhost:8480/resource-usage/${user}`).then(function (response) {
                    setData(response.data);
                })
            })
    }

    async function onEdit(rowData) {
        //Prepopulate values from flight
        const distance = document.getElementById("amount_edit");
        const usageDate = document.getElementById("usageDate_edit");
        const householdCount = document.getElementById("householdCount_edit");
        // Create a unique id
        const id = document.getElementById("usageId_edit");
        const resourceType = document.getElementById("resourceType_edit");
        const unitOfMeasurement = document.getElementById("unitOfMeasurement_edit");
        distance.setAttribute("value", rowData.getValue("amount"));
        usageDate.setAttribute("value", rowData.getValue("date"));
        householdCount.setAttribute("value", rowData.getValue("householdCount"));
        id.setAttribute("value", rowData.getValue("edit"));
        resourceType.setAttribute("value", rowData.getValue("resourceType"));
        unitOfMeasurement.setAttribute("value", rowData.getValue("unitOfMeasurement"));

        document.getElementById("my_edit_modal").showModal()
    }

    return (
        <tr className={rowData.resourceType}>
            <td className="p-5 border border-b-slate-700">{rowData.getValue("amount")}</td>
            <td className="p-5 border border-b-slate-700">{rowData.getValue("householdCount")}</td>
            <td className="p-5 border border-b-slate-700">{rowData.getValue("date")}</td>
            <td className="p-5 border border-b-slate-700">
                <label htmlFor="my_modal_6" className="btn" onClick={() => onEdit(rowData)}>Edit</label>
                <input type="checkbox" id="my_modal_6" className="modal-toggle"/>
            </td>
            <td className="p-5 border border-b-slate-700"><button className="btn" onClick={() => {onDelete(rowData.getValue("edit"), rowData.getValue("resourceType"))}}>Delete</button></td>
        </tr>
    );
}

function UsagesTable({user, table, setData}) {
    const rows = [];
    table.getRowModel().rows.forEach((row) => {
        rows.push(
            <UsageRow
                user={user}
                rowData={row}
                setData={setData}
            />
        );
    });

    function onClickFilter(resourceTypeToFilterBy) {
        const resourceTypeColumn = table.getColumn('resourceType');
        resourceTypeColumn.setFilterValue(resourceTypeToFilterBy);
    }

    async function onEditSubmit() {
        // Gather data about usages from inputs
        // TODO: Wrap inputs in a form, use onSubmit, pass it a function, parse function data based on name
        const distance = document.getElementById("amount_edit").value;
        const usageDate = document.getElementById("usageDate_edit").value;
        const householdCount = document.getElementById("householdCount_edit").value;
        // Create a unique id
        const id = document.getElementById("usageId_edit").value;
        const resourceType = document.getElementById("resourceType_edit").value;
        const unitOfMeasurement = document.getElementById("unitOfMeasurement_edit").value;
        // Post to /flight
        await axios.put(`http://localhost:8480/resource-usage/${user}/${id}`, {
            "amount": distance,
            "unitOfMeasurement": unitOfMeasurement,
            "householdCount": householdCount,
            "date": usageDate,
            "userId": user,
            "resourceType": resourceType
        })
        let getResponse = await axios.get(`http://localhost:8480/resource-usage/${user}`);
        setData(getResponse.data);
    }

    return (
        <div className="p-9 basis-1/2">
            <table className="table-auto border-collapse ">
                <thead>
                <tr>
                    <th className="border border-slate-700 p-5">Amount</th>
                    <th className="border border-slate-700 p-5">Household</th>
                    <th className="border border-slate-700 p-5">Date</th>
                    <th className="border border-slate-700 p-5">Edit</th>
                    <th className="border border-slate-700 p-5">Delete</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>

            </table>
            <div className="space-x-5">
                <button className="btn" value="water" onClick={() => { onClickFilter("water")}}>Water</button>
                <button className="btn" value="electricity" onClick={() => { onClickFilter("electricity")}}>Electricity</button>
                <button className="btn" value="natural_gas" onClick={() => { onClickFilter("natural_gas")}}>Natural Gas</button>
            </div>
            <dialog id="my_edit_modal" className="modal overflow-y-scroll">
                <div className="modal-box">
                    <div>
                        <label className="label">Total Amount*</label>
                        <input className="input input-bordered input-accent" type="number" id="amount_edit" placeholder="Enter Amount*"/>
                    </div>
                    <div>
                        <label className="label">Usage Date*</label>
                        <input type="date" id="usageDate_edit" placeholder="Usage Date*"/>
                    </div>
                    <div>
                        <label className="label">Household Count*</label>
                        <input className="block input input-accent input-bordered" type="number" id="householdCount_edit" value="5" placeholder="Household Count*"/>
                    </div>
                    <input className="input" hidden={true} id="usageId_edit" />
                    <input className="input" hidden={true} id="resourceType_edit" />
                    <input className="input" hidden={true} id="unitOfMeasurement_edit" />
                    <div className="modal-action">
                        <form method="dialog">
                            <button className="btn bg-green-700 hover:bg-green-600" onClick={onEditSubmit}>Submit</button>
                        </form>
                    </div>
                </div>
            </dialog>
        </div>
    );
}

function AddUsageSection({user, setData}) {
    function onClick() {
        // Gather data about usage from inputs
        const amount = document.getElementById("amount").value;
        const resourceType = document.getElementById("type").value;
        const unitOfMeasurement = document.getElementById("unitOfMeasurement").value;
        const usageDate = document.getElementById("usageDate").value;
        const householdCount = document.getElementById("householdCount").value;
        // Create a unique id
        const id = uuidv4();
        // Post to /resource-usage
        axios.post("http://localhost:8480/resource-usage", {
            "amount": amount,
            "resourceType": resourceType,
            "unitOfMeasurement": unitOfMeasurement,
            "householdCount": householdCount,
            "date": usageDate,
            "id": id,
            "userId": user
        }).then(function () {
            // GET all /resource-usage for user and update individual resource states
            axios.get(`http://localhost:8480/resource-usage/${user}`).then(function(response){
                setData(response.data)
            })
        });
    }

    return (
        <div className="p-9 basis-1/4">
            <div>
                <label className="label">Resource Type*</label>
                <select className="select select-accent select-bordered" id="type">
                    <option value="water">Water</option>
                    <option value="electricity">Electricity</option>
                    <option value="natural_gas">Gas</option>
                </select>
            </div>
            <div>
                <label className="label">Total Amount*</label>
                <input className="input input-bordered input-accent" type="number" id="amount" placeholder="Enter Amount*"/>
            </div>
            <div>
                <label className="label">Resource Type*</label>
                <select className="select select-accent select-bordered" id="unitOfMeasurement">
                    <option className="water" value="gallons">gallons</option>
                    <option className="water" value="ccf">ccf</option>
                </select>
            </div>
            <div>
                <label className="label">Usage Date*</label>
                <input type="date" id="usageDate" placeholder="Usage Date*"/>
            </div>
            <div>
                <label className="label">Household Count*</label>
                <input className="block input input-accent input-bordered" type="number" id="householdCount" value="5" placeholder="Household Count*"/>
            </div>
            <button className="btn bg-green-700 hover:bg-green-600 mt-5" onClick={onClick}>Submit</button>
        </div>
    );
}

function UsagesPage({setData, user, table}) {

    return (
        <div className="flex flex-wrap flex-row overflow-y-auto ">
            <AddUsageSection
                user={user}
                setData={setData}
            />
            <UsagesTable
                user={user}
                table={table}
                setData={setData}
            />
        </div>
    );
}

export default function Usages() {
    const [user, setUser] = useState("1");
    const [data, setData] = useState([]);
    //Load data
    useEffect(() => {
        axios.get(`http://localhost:8480/resource-usage/${user}`).then(function (response) {
            // console.log(response.data);
            //Add all resources to table data
            setData(response.data);
        });
    }, [setData]);

    const columnHelper = createColumnHelper();
    const defaultColumns = [
        columnHelper.accessor('amount', {
            cell: info => info.getValue(),
            footer: props => props.column.id,
        }),
        columnHelper.accessor('resourceType', {
            cell: info => info.getValue(),
            footer: props => props.column.id,
            filterFn: 'resourceTypeFilter'
        }),
        columnHelper.accessor('householdCount', {
            cell: info => info.getValue(),
            footer: props => props.column.id,
        }),
        columnHelper.accessor('date', {
            cell: info => info.getValue(),
            footer: props => props.column.id,
        }),
        columnHelper.accessor(row => `${row.id}`,{
            id: 'edit',
        }),
        columnHelper.accessor(row => `${row.id}`, {
            id: 'delete',
        }),
    ]

    const table = useReactTable({
        data: data,
        columns: defaultColumns,
        filterFns: {
            'resourceTypeFilter': (row, columnId, filterValue) => {
                return filterValue === row.getValue("resourceType");
            }
        },
        getCoreRowModel: getCoreRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        getSortedRowModel: getSortedRowModel()
    });

    return <UsagesPage
        setData={setData}
        user={user}
        table={table}
    />;
}
