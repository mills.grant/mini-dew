/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {},
    },
    daisyui: {
        themes: [
            "nord",
            "lemonade",
            "dim",
            "business",
            "lofi",
            {
                custom: {
                    primary: "#3a506b",
                    secondary: "#5BC0BE",
                    accent: "#0891b2",
                    neutral: "#001219",
                    "base-100": "#e7e5e4",
                    info: "#0891b2",
                    success: "#10b981",
                    warning: "#d97706",
                    error: "#f43f5e",
                },
            },
        ],
        darkTheme: "custom",
    },
    plugins: [require("daisyui")],
}
